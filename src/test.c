#define _DEFAULT_SOURCE

#include "test.h"

#include "mem.h"
#include "mem_internals.h"

void heap_destroy(void* addr, size_t heap_size) {
  size_t heap_actual_size = size_from_capacity((block_capacity) { heap_size }).bytes;
  munmap(addr, heap_actual_size);
}

// Обычное успешное выделение памяти.
void heap_test_1(void) {
  fprintf(stderr, "Test #1:\n");
  void* heap_start = heap_init(REGION_MIN_SIZE);

  fprintf(stderr, "Heap state before _malloc(123):\n");
  debug_heap(stderr, heap_start);

  if (!_malloc(123)) {
    fprintf(stderr, "Test #1 failed\n");
    return;
  }

  fprintf(stderr, "Heap state after _malloc(123):\n");
  debug_heap(stderr, heap_start);

  heap_destroy(HEAP_START, REGION_MIN_SIZE);
  fprintf(stderr, "Test #1 succeeded\n");
}

// Освобождение одного блока из нескольких выделенных.
void heap_test_2(void) {
  fprintf(stderr, "Test #2:\n");
  void* heap_start = heap_init(REGION_MIN_SIZE);

  fprintf(stderr, "Heap state before _malloc's:\n");
  debug_heap(stderr, heap_start);

  void *before = _malloc(123);
  void *to_be_freed = _malloc(1488);
  void *after = _malloc(1337);
  if (!before || !to_be_freed || !after) {
    fprintf(stderr, "Test #2 failed\n");
    return;
  }

  fprintf(stderr, "Heap state after _malloc's:\n");
  debug_heap(stderr, heap_start);

  _free(to_be_freed);

  fprintf(stderr, "Heap state after _free:\n");
  debug_heap(stderr, heap_start);

  heap_destroy(HEAP_START, REGION_MIN_SIZE);
  fprintf(stderr, "Test #2 succeeded\n\n");
}

// Освобождение двух блоков из нескольких выделенных.
void heap_test_3(void) {
  fprintf(stderr, "Test #3:\n");
  void* heap_start = heap_init(REGION_MIN_SIZE);

  fprintf(stderr, "Heap state before _malloc's:\n");
  debug_heap(stderr, heap_start);

  void* before = _malloc(123);
  void* to_be_freed_1 = _malloc(1488);
  void* to_be_freed_2 = _malloc(1337);

  if (!before || !to_be_freed_1 || !to_be_freed_2) {
    fprintf(stderr, "Test #3 failed\n");
    return;
  }

  fprintf(stderr, "Heap state after _malloc's:\n");
  debug_heap(stderr, heap_start);

  _free(to_be_freed_1);
  _free(to_be_freed_2);

  fprintf(stderr, "Heap state after _free's:\n");
  debug_heap(stderr, heap_start);

  heap_destroy(HEAP_START, REGION_MIN_SIZE);
  fprintf(stderr, "Test #3 succeeded\n\n");
}

// Память закончилась, новый регион памяти расширяет старый.
void heap_test_4(void) {
  fprintf(stderr, "Test #4:\n");
  void* heap_start = heap_init(REGION_MIN_SIZE);

  fprintf(stderr, "Heap state before _malloc(8000):\n");
  debug_heap(stderr, heap_start);

  void* big = _malloc(8000);

  if (!big) {
    fprintf(stderr, "Test #4 failed (couldn't allocate big block)\n");
    return;
  }

  fprintf(stderr, "Heap state after _malloc(8000):\n");
  debug_heap(stderr, heap_start);

  void* small = _malloc(500);

  if (!small || small != big + 8000) {
    fprintf(stderr, "Test #4 failed (couldn't extend heap)\n");
    return;
  }

  fprintf(stderr, "Heap state after _malloc(500):\n");
  debug_heap(stderr, heap_start);

  heap_destroy(heap_start, REGION_MIN_SIZE);
  heap_destroy(heap_start + REGION_MIN_SIZE, 500);
  fprintf(stderr, "Test #4 succeeded\n");
}

// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
void heap_test_5(void) {
  fprintf(stderr, "Test #4:\n");
  void* heap_start = heap_init(REGION_MIN_SIZE);

  fprintf(stderr, "Heap state before _malloc(8000):\n");
  debug_heap(stderr, heap_start);

  void* big = _malloc(8000);

  if (!big) {
    fprintf(stderr, "Test #5 failed (couldn't allocate big block)\n");
    return;
  }

  fprintf(stderr, "Heap state after _malloc(8000):\n");
  debug_heap(stderr, heap_start);

  (void) mmap( (void*) heap_start + REGION_MIN_SIZE, 1, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS , -1, 0);

  void* small = _malloc(500);

  if (!small || small == big + 8000) {
    fprintf(stderr, "Test #5 failed (new block extends heap)\n");
    return;
  }


  fprintf(stderr, "Heap state after _malloc(500):\n");
  debug_heap(stderr, heap_start);

  fprintf(stderr, "Test #5 succeeded\n");
}
